package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;
import javafx.geometry.Pos;

/**
 * Created by Hari on 12/16/2014.
 */
public class Player {
    private Vector2 Velocity;
    private Vector2 Position;
    private boolean isfacingleft;
    private boolean ismoving;
    public static final float MAXVelocity = 1.9f;
    private states defaultstate;

    public Player() {
        Velocity = new Vector2();
        Position = new Vector2();
    }
    public void setPostion(Vector2 setposition) {
        this.Position = setposition;
    }
    public Vector2 obtainPosition(){
        return Position;

    }
    public Vector2 obtainVelocity() {
        return Velocity;
    }
    enum states {
        IDLE,RIGHT,JUMP,ATTACKLEFT,ATTACKRIGHT,DEAD,LEFT
    }
    public void setState(states defaultstate1) {
        this.defaultstate = defaultstate1;
    }
    public states returnstate() {
        return defaultstate;
    }



}
