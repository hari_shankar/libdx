package com.mygdx.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;


/**
 * Created by Hari on 12/13/2014.
 */
public class Level {
    private TiledMap level;
    private float width;
    private float height;
    BodyDef blocks;
    private MyGdxGame game;
    private String levelname1;
    private FixtureDef fixtureDef;
    private TiledMapRenderer tmr;
    private Box2DDebugRenderer render;

    //@param levelname and world
    public Level(String levelname, World world) {
        this.levelname1 = levelname;
        game = new MyGdxGame();
        level = new TmxMapLoader().load(levelname1);
        TiledMapTileLayer layer = (TiledMapTileLayer) level.getLayers().get(0);
        width = layer.getWidth();
        height = layer.getHeight();
        blocks = new BodyDef();
        fixtureDef = new FixtureDef();

        //create cells
        for (int x = 0; x < getHeight(); x++) {
            for (int y = 0; y < getWidth(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                if (cell == null) continue;
                if (cell.getTile() == null) continue;
                //create bodties

                blocks.type = BodyDef.BodyType.StaticBody;
                blocks.position.set((x + 0.5f) * getWidth() / game.PtM, (y + 0.5f) * getHeight() / game.PtM);
                //creating chainshape, no need to use Physicstuff for fixtures as we are using blocks
                ChainShape cshape = new ChainShape();
                Vector2[] vector2 = new Vector2[3];
                //set defintions for vectors
                vector2[0] = new Vector2(-getWidth() / 2 / game.PtM, -getHeight() / 2 / game.PtM);
                vector2[1] = new Vector2(-getWidth() / 2 / game.PtM, getHeight() / 2 / game.PtM);
                vector2[2] = new Vector2(getWidth() / 2 / game.PtM, getHeight() / 2 / game.PtM);

                cshape.createChain(vector2);
                fixtureDef.friction = 0;
                fixtureDef.shape = cshape;
                fixtureDef.density = 0;
                fixtureDef.restitution = 0;
                fixtureDef.isSensor = false;
                fixtureDef.filter.categoryBits = 2;
                fixtureDef.filter.maskBits = -1;
                world.createBody(blocks).createFixture(fixtureDef);


            }


        }
        tmr = new OrthogonalTiledMapRenderer(level);

    }

    public TiledMap level() {
        return level;

    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public void drawBlocks(OrthographicCamera camera) {
        tmr.setView(camera);
        tmr.render();



    }


}





