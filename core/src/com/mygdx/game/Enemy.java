package com.mygdx.game;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Hari on 12/14/2014.
 */
public class Enemy {
    private int health;
    private float damage;
    private Vector2 position;
    private Vector2 velocity;
    private boolean rightmove;
    private enemystate enemystate;

    enum enemystate {
        IDLE,LEFT,RIGHT,ATTACKSTAND,ATTACKLEFT,ATTACKRIGHT

    }
    public void setEnemystate(enemystate estate) {
        this.enemystate = estate;
    }
    public enemystate getEnemystate() {
        return enemystate;
    }
    public boolean isEnemyRight() {
        return rightmove;
    }
    public void setEnemyRight(boolean eright) {
        this.rightmove = eright;
    }
    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }
    public Vector2 obtainEnemyVelocity() {
        return velocity;
    }
    public void setEnemyPosition(Vector2 position) {
        this.position = position;

    }
    public void setHealth(int hp) {
        this.health = hp;
    }
    public int getHealth() {
        return health;
    }
    public void setDamage(int d){
        this.damage = d;
    }
    public float Damage(){
        return damage;

    }

    public Enemy(Vector2 initialpos) {
        this.position   = initialpos;
        velocity = new Vector2();
        enemystate = enemystate.IDLE;
        rightmove = true;
        health = 100;
        damage = 10;
    }



}
