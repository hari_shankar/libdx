package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Hari on 12/10/2014.
 */
public class Player {
    public static final float speed = 50f;
    public static final float slowdown = 0.87f;
    public Vector2 Velocity;
    public Vector2 Position;
    PlayerState playerstate;
    private boolean isNotJumping = true;
    private boolean moveleft;
    private boolean ismoving;
    private float deltastate;
    float stateTime = 0;

    public Player() {
        playerstate = PlayerState.IDLE;
        Velocity = new Vector2();
        Position = new Vector2();
        deltastate = 0;
        ismoving = false;
    }
public void update(float dt){
    dt += Gdx.graphics.getDeltaTime();
    deltastate += dt;
    obtainPosition().x += obtainVelocity().x * dt ;

}
    public void setPlayerState(PlayerState state) {
        this.playerstate = state;
    }

    public void setVelocity(Vector2 velocity) {

        this.Velocity = velocity;
    }

    public Vector2 obtainVelocity() {
        return Velocity;
    }

    public void setPos(Vector2 pos) {
        this.Position = pos;

    }

    public Vector2 obtainPosition() {

        return Position;
    }

    public PlayerState getStat() {
        return playerstate;
    }

    public boolean amiJumpining() {
        return isNotJumping;
    }

    enum PlayerState {
        LEFT, RIGHT, JUMP, IDLE,
    }
public void setleft(boolean faceleftorbust) {
    this.moveleft = faceleftorbust;

}
    public boolean isFacingLeft() {
        return  moveleft;
    }
    public void Setmove(boolean amImoving) {
        this.ismoving = amImoving;
    }
    public boolean isMoving() {
        return ismoving;
    }
    public float getStateTime(){
        return deltastate;
    }


}

