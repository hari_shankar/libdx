package com.mygdx.game;

import android.graphics.Path;
import aurelienribon.bodyeditor.BodyEditorLoader;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;



/**
 * Created by Hari on 12/19/2014.
 */
public class PhysicsStuff {

    BodyDef defs;
    Body body1;
    FixtureDef fdef;
    BodyDef.BodyType typerino;
    float friction;
    float density;
    float restituion;
    PolygonShape shape;
    BodyEditorLoader loader;
    private String pathString;
    float scale;
    private World world;
    private String fix_name;
private BodyDef.BodyType bodyType;

    public void  addStuff(String path,float friction,float restituion,float density,String name,Body body, FixtureDef fixtureDef, float scale,BodyEditorLoader loader_p) {
        this.pathString = path;
        this.fix_name = name;
        fdef = new FixtureDef();
        this.scale = scale;
        setFriction(friction);
        setDensity(density);
        setRestituion(restituion);
        this.body1 = body;
        this.fdef =fixtureDef;
        this.loader = loader_p;
        loader = new BodyEditorLoader(Gdx.files.internal(pathString));
        loader.attachFixture(body1,fix_name,fixtureDef,scale);
        this.fdef.filter.maskBits = -1;
        this.fdef.filter.categoryBits = 3;





    }


public void createBody(BodyDef.BodyType type, Body body, World world ) {
    this.world = world;

    this.bodyType = type;
    bodyType = defs.type;
    


  }







    public float getFriction() {
        return  friction;
    }
    public float getDensity() {
        return density;
    }
    public float getRestituion() {
        return restituion;
    }
public void createShape(PolygonShape shape) {
    shape.setAsBox(64/8,64/8);
    fdef.shape = shape;
}
    public void setPath(String Path) {
        this.pathString = Path;

    }
    public String getPath() {
        return  pathString;
    }
    public void  setScale(float scale) {
        this.scale = scale;

    }
 public void setFriction(float friction1) {
     this.friction = friction1;
     friction = fdef.friction;
 }
    public void setDensity(float density1) {
        this.density = density1;
        density = fdef.density;

    }
    public void setRestituion(float restituion1) {
        this.restituion = restituion1;
        restituion = fdef.restitution;
    }




}
