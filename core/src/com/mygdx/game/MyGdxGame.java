package com.mygdx.game;

import aurelienribon.bodyeditor.BodyEditorLoader;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;


public class MyGdxGame extends ApplicationAdapter implements InputProcessor {
    SpriteBatch batch;
    Box2DDebugRenderer renderer;
    TextureRegion guyidle;
    TextureRegion guyidleleft;
    TextureAtlas playeranims;
    Player player = new Player();
    Animation walkleft;
    Animation walkright;
    TextureRegion drawplayer;
    Texture texture;
    Body playerbody;
    FixtureDef playerfixtureCollisoon;
    BodyDef player1;
    Fixture PlayerDef;
    private Level level;
    World world;
    private  OrthographicCamera cam;
private OrthogonalTiledMapRenderer tmr;

    // PolygonShape polygonshape1;
    public final float PtM = 100;
    private OrthographicCamera camera;
    Box2DDebugRenderer renderz;
    BodyEditorLoader loader;
    String name;
    PhysicsStuff stuff;
    private Vector2 jack;


    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.LEFT) {
            player.setPlayerState(player.playerstate.LEFT);
        }
        if (keycode == Input.Keys.RIGHT) {
            player.setPlayerState(player.playerstate.RIGHT);
        }

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {

        if (keycode == Input.Keys.RIGHT || keycode == Input.Keys.LEFT) {
            player.setPlayerState(player.playerstate.IDLE);
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void create() {
        world = new World(new Vector2(0, -10f), true);
        player1 = new BodyDef();
        player1.type = BodyDef.BodyType.DynamicBody;
        playerfixtureCollisoon = new FixtureDef();
        playerbody = world.createBody(player1);
        playerbody.setUserData(playerbody);

     playerbody.setGravityScale(PtM);


        playerfixtureCollisoon.filter.categoryBits = 1;
        playerfixtureCollisoon.filter.maskBits = -1;
        stuff = new PhysicsStuff();
        stuff.addStuff("android/assets/data/GUy.json", 0, 0, 0, "Guy", playerbody, playerfixtureCollisoon, PtM, loader);
        camera = new OrthographicCamera();
        renderz = new Box2DDebugRenderer();
        cam = new OrthographicCamera();
        cam.setToOrtho(false, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        cam.update();
        playerbody.setTransform(0,300,0);
        level = new Level("android/assets/data/Maps/lel.tmx",world);
        camera.setToOrtho(false, Gdx.graphics.getWidth() / PtM, Gdx.graphics.getHeight() / PtM);


//    playerbody.createFixture(playerfixtureCollisoon);


        //level = new Level("level1.tmx");
        loadTextures();
        batch = new SpriteBatch();
        Player player = new Player();


        Gdx.input.setInputProcessor(this);



    }

    @Override
    public void render() {

player1.position.set( new Vector2(0,100));

        float dt = Gdx.graphics.getDeltaTime();
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.step(1f / 60f, 10, 2);
        cam.update();

     level.drawBlocks(cam);

        batch.begin();

        camera.update();
        renderz.render(world, camera.combined);
        batch.end();
        drawPlayer();
        updatePlayer(dt);


        player.update(dt);
    }

    public void updatePlayer(float dt) {

        if (player.getStat() == player.playerstate.LEFT) {
            player.obtainVelocity().x = -player.speed;
            player.setleft(true);
            player.Setmove(true);


        }
        if (player.getStat() == player.playerstate.RIGHT) {
            player.obtainVelocity().x = player.speed;
            player.setleft(false);
            player.Setmove(true);
        }
        if (Math.abs(player.obtainVelocity().x) > player.speed) {
            player.obtainVelocity().x = Math.signum(player.obtainVelocity().x) * Player.speed;

        }

        if (Math.abs(player.obtainVelocity().x) < 1) {
            player.setPlayerState(player.playerstate.IDLE);
            player.obtainVelocity().x = 0;


        }
        if (player.getStat() == player.playerstate.IDLE) {
            player.obtainVelocity().x = 0;
            player.Setmove(false);
        }


        //player.obtainVelocity().scl(dt);\

        //  playerbody.getPosition().x += player.obtainVelocity().x * dt ;
        playerbody.setLinearVelocity(player.obtainVelocity().x / PtM, player.obtainVelocity().y / PtM);
        //player.obtainVelocity().scl(1/dt);
        //player.obtainVelocity().x *= player.slowdown;
        System.out.println(playerbody.getLinearVelocity().x);
    }


    public void loadTextures() {
        playeranims = new TextureAtlas("android/assets/data/walkcyle.atlas");
        TextureRegion[] walkcycle = new TextureRegion[5];
        TextureRegion[] wallkcyclert = new TextureRegion[5];

       /*walkcycle[0]= playeranims.findRegion("1");
        walkcycle[1] = playeranims.findRegion("2");
        walkcycle[2] = playeranims.findRegion("3");
        walkcycle[3] = playeranims.findRegion("4");
        walkcycle[4] = playeranims.findRegion("5"); */ //works

        for (int i = 0; i <= 4; i++) {
            wallkcyclert[i] = playeranims.findRegion(Integer.toString(i + 1));

        }
        for (int i = 0; i <= 4; i++) {
            walkcycle[i] = new TextureRegion(wallkcyclert[i]);
            walkcycle[i].flip(true, false);
        }


        guyidle = playeranims.findRegion("1");
        walkleft = new Animation(1 / 10f, walkcycle);
        walkright = new Animation(1 / 10f, wallkcyclert);


    }

    public void drawPlayer() {
        SpriteBatch draw = new SpriteBatch();

        if (player.isMoving() && player.isFacingLeft()) {
            drawplayer = walkleft.getKeyFrame(player.getStateTime(), true);
        }
        if (player.isMoving() && !player.isFacingLeft()) {
            drawplayer = walkright.getKeyFrame(player.getStateTime(), true);
        }
        if (!player.isMoving() && !player.isFacingLeft()) {
            drawplayer = guyidle;
        }
        if (!player.isMoving() && player.isFacingLeft()) {
            guyidleleft = new TextureRegion(guyidle);
            guyidleleft.flip(true, false);
            drawplayer = guyidleleft;

        }


        draw.begin();

        //draw.draw(drawplayer, player.obtainPosition().x, player.obtainPosition().y);

        draw.draw(drawplayer, playerbody.getPosition().x * PtM, playerbody.getPosition().y);

//playerbody.setGravityScale(PtM);

        draw.end();


    }

    public void loadEnemy() {

    }

    @Override
    public void dispose() {
        super.dispose();


    }


}

